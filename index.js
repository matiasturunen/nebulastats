const fetch = require('node-fetch');
const {InfluxDB, Point, HttpError} = require('@influxdata/influxdb-client')

const influxUrl = process.env.INFLUXDB_URL || '';
const influxApiToken = process.env.INFLUXDB_TOKEN || '';
const influxOrg = process.env.INFLUXDB_ORG || '';
const influxBucket = process.env.INFLUXDB_BUCKET || '';

const influxDB = new InfluxDB({url: influxUrl, token: influxApiToken});
const writeApi = influxDB.getWriteApi(influxOrg, influxBucket);

const createPoint = (row) => {
	const point = new Point(row.n)
		.floatField(row.u, row.v);
	writeApi.writePoint(point);
};

const fetcher = () => {
	prevFetch = '';
	setInterval(() => {
		fetch('https://aquasuite.aquacomputer.de/view/035bd21a-8e22-469e-b035-272bb27d9a6a')
			.then(response => response.json())
			.then(data => {
				if (prevFetch == data.t) {
					console.log('Data not changed!');
				} else {
					prevFetch = data.t;
					console.log(data);
					for (var i = 0; i < data.d.length; i++) {
						createPoint(data.d[i])
					}
					return writeApi.flush();
				}
			})
			.then((a) => console.log('Flushed: ', a))
			.catch(err => console.error(err));
	}, 15000);
};

fetcher();
//writeApi.close();
